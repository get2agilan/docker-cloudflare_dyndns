import requests
import json
import datetime
import os
import time
import socket

# Retrieve Cloudflare Credentials, ZoneID, Domain from Environmental Variables
cloudflare_email = os.environ['CLOUDFLARE_EMAIL']
cloudflare_token = os.environ['CLOUDFLARE_TOKEN']
cloudflare_dyndns = os.environ['CLOUDFLARE_DYNDNS']

# Authentication Header
headers = {
    'X-Auth-Email': cloudflare_email,
    'X-Auth-Key': cloudflare_token,
    'Content-Type': 'application/json',
}

# Cloudflare API Endpoint URLs
cloudflare_api_url = 'https://api.cloudflare.com/client/v4/zones/'

while True:
    # Retrieve Zone ID from Cloudflare
    try:
        response = requests.get(cloudflare_api_url, headers=headers)
        response.raise_for_status()
    except requests.exceptions.RequestException as err:
        print ("%s \t Connection Error - Unable to Retrieve Zone ID - Restarting 10 minutes."% (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        time.sleep(600)
        break
    result = json.loads(response.text)
    cloudflare_zone_id = result['result'][0]['id']

    # Cloudflare API - Zone URL
    cloudflare_api_dns_records_url = cloudflare_api_url + cloudflare_zone_id + "/dns_records/"

    # Retrieve Cloudflare DynDNS Domain ID and IP Address from Cloudflare
    params = (
        ('type', 'A'),
        ('name', cloudflare_dyndns),
    )

    try:
        response = requests.get(cloudflare_api_dns_records_url, headers=headers, params=params)
        response.raise_for_status()
    except requests.exceptions.RequestException as err:
        print ("%s \t Connection Error - Unable to Retrieve Domain IP - Restarting in 10 minutes."% (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        time.sleep(600)
        break
    result = json.loads(response.text)
    cloudflare_ip = result['result'][0]['content']
    cloudflare_id = result['result'][0]['id']

    # Cloudflare API - DynDNS Domain URL
    cloudflare_api_dns_update_url = cloudflare_api_dns_records_url + cloudflare_id

    while True:
        # Retrieve Public IP Address
        try:
            response = requests.get('http://checkip.amazonaws.com/')
            response.raise_for_status()
        except requests.exceptions.RequestException as err:
            print ("%s \t Connection Error - Unable Resolve Public IP - Restarting in 10 minutes."% (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
            time.sleep(600)
            break
        external_ip = response.text.strip()

        # Retrieve DynDNS IP Address
        cloudflare_ip = socket.gethostbyname(cloudflare_dyndns)

        print ("%s \t External IP : %s \t CloudFlare IP : %s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), external_ip, cloudflare_ip))

        # Update Cloudflare DynDNS IP if External IP doesn't match Cloudflare IP Address
        if cloudflare_ip != external_ip :
            data = '{"type":"A","name":"' + cloudflare_dyndns + '","content":"' + external_ip + '"}'
            try:
                response = requests.put(cloudflare_api_dns_update_url, headers=headers, data=data)
                response.raise_for_status()
            except requests.exceptions.RequestException as err:
                print ("%s \t Connection Error - Unable Update CloudFlare DNS - Restarting in 10 minutes."% (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
                time.sleep(600)
                break
            print ("%s \t Updating DNS Entry " % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))

        # Sleep for an hour
        time.sleep(3600)

### To Do
# 1. CloudFlare Account with Multiple Accounts | cloudflare_domain = os.environ['CLOUDFLARE_DOMAIN']
# 2. Exception Handling
