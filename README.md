Quick Start:

```bash
docker run --name DynDNS -d \
  -e CLOUDFLARE_EMAIL=<Email> \
  -e CLOUDFLARE_TOKEN=<API-Token> \
  -e CLOUDFLARE_DYNDNS=<Subdomain> \
  --restart unless-stopped \
  get2agilan/cloudflare_dyndns:latest
```

Python file can be run locally by setting the following environmental variables:
- CLOUDFLARE_EMAIL
- CLOUDFLARE_TOKEN
- CLOUDFLARE_DYNDNS
